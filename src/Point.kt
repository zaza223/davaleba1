import kotlin.math.pow

fun main() {
    //Point class tests
    println("\nPoint tests")
    val p1: Point = Point(3.0, 4.0)
    val p2: Point = Point(3.0, 4.0)
    val p3: Point = Point(-6.0, 8.0)
    println(p1.toString())
    println(p1==p2)
    println(p1==p3)
    println(p1.distanceFrom(p2))
    p3.makeOriginSymmetry()
    println(p3.toString())

    //Fraction class tests
    println("\nFraction tests")
    val f1: Fraction = Fraction(3, 7)
    val f2: Fraction = Fraction(5, 5)
    val f3: Fraction = Fraction(-18, -6)

    println(f1.toString() + "\n" + f2.toString())
    println(f1.add(f2).toString())
    val newF: Fraction = f1.add(f2)
    println(f1.multiply(f2).toString())
    newF.reduceToLowest()
    println(newF.toString())
    f2.reduceToLowest()
    println(f2.toString())
    f3.reduceToLowest()
    println(f3.toString())
}

/*
 * class includes methods toString, equals, makeOriginSymmetry, distanceFrom
 */
class Point(private var xCoordinate: Double, private var yCoordinate: Double) {

    override fun toString(): String {
        return "coordinates: ($xCoordinate ; $yCoordinate)"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Point) {
            return other.xCoordinate == xCoordinate && other.yCoordinate == yCoordinate
        }
        return false
    }

    //changes point coordinates to negative
    fun makeOriginSymmetry() {
        xCoordinate *= -1
        yCoordinate *= -1
    }

    //calculates distance between 2 points and returns distance
    fun distanceFrom(pt: Point): Double {
        return ((pt.xCoordinate - this.xCoordinate).pow(2) + (pt.yCoordinate - this.yCoordinate).pow(2)).pow(0.5)
    }
}

/*
 * fraction class, includes methods ToString, Equals, Add and Multiply
 */
class Fraction(private var numerator: Int, private var denominator: Int) {

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            return other.numerator * this.numerator == other.denominator * this.denominator
        }
        return false
    }

    override fun toString(): String {
        return "$numerator / $denominator"
    }

    /*
     * method finds the greatest common divisor except 1 and changes numerator and denominator accordingly
     */
    fun reduceToLowest() {
        val gcf: Int = findGCF(numerator, denominator)
        //when both numbers are negative, minus signs are eliminated
        if(numerator<0&&denominator<0){
            numerator*=-1
            denominator*=-1
        }
        if (gcf != 1) {
            numerator /= gcf
            denominator /= gcf
        }
    }

    private fun findGCF(n: Int, d: Int): Int {
        var gcf = 1
        var min: Int = n
        if (n > d) min = d


        for (a in 2..Math.abs(min)) {
            if (numerator % a == 0 && denominator % a == 0) {
                gcf = a
            }
        }
        return gcf
    }

    fun add(f: Fraction): Fraction {
        val newD: Int = denominator * f.denominator
        val newN: Int = f.denominator*numerator + denominator*f.numerator

        return Fraction(newN, newD)
    }

    fun multiply(f: Fraction): Fraction {
        val newN: Int = numerator * f.numerator
        val newD: Int = denominator * f.denominator
        return Fraction(newN, newD)
    }
}
